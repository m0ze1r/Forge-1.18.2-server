#!/bin/bash
echo "Getting MC-forge jar"
sleep 1
wget https://maven.minecraftforge.net/net/minecraftforge/forge/1.18.2-40.1.31/forge-1.18.2-40.1.31-installer.jar
sleep 2
echo "Installing Server package"
java -jar forge-1.18.2-40.1.31-installer.jar --installServer
echo "First Time Run"
sleep 2
./run.sh
echo "Catting out eula.txt"
sleep 5
cat <<EOF > eula.txt
eula=true
EOF
echo "Sleeping for safety sake"
sleep 5
echo "Second Time Run w/ Eula, Good Luck!"
./run.sh
